package com.sda.back.example.views.windows;

import com.sda.back.example.controller.TaskController;
import com.sda.back.example.controller.UserController;
import com.sda.back.example.model.Task;
import com.sda.back.example.model.User;
import com.sda.back.example.views.SessionCookies;

import javax.swing.*;
import java.awt.*;
import java.util.List;

public class WindowFrame extends JFrame implements ILoginListener, IRegisterListener, ICardSwitchingListener, ITaskListener {
    private UserController userController = new UserController();
    private TaskController taskController = new TaskController();
    private SessionCookies cookies = new SessionCookies();

    public final static String CARD_LOGIN = "LOGIN";
    public final static String CARD_APP = "APP";
    public final static String CARD_REGISTER = "REGISTER";


    private CardLayout layout;

    private JPanel mainPanel;
//    private String currentCard = CARD_LOGIN;
    private String currentCard = CARD_REGISTER;

    public WindowFrame() {
        super();

        setContentPane(mainPanel);

        setMinimumSize(new Dimension(640, 400));
        setPreferredSize(new Dimension(640, 400));
        setSize(new Dimension(640, 400));


        layout = (CardLayout) mainPanel.getLayout();

        mainPanel.add(new RegisterForm(this,this).getMainPanel(),CARD_REGISTER);
        mainPanel.add(new LoginForm(this, this).getMainPanel(), CARD_LOGIN);
        mainPanel.add(new ApplicationView(this,this).getMainPanel(), CARD_APP);


        layout.show(mainPanel, CARD_LOGIN);
    }

    /**
     * Metoda służy do przełaczania kart.
     */
//    public void switchCard(){
//        if (currentCard.equals(CARD_LOGIN)) {
//            switchCard(CARD_APP);
//        }else if(currentCard.equals(CARD_REGISTER)) {
//            switchCard(CARD_LOGIN);
//        }else{
//            switchCard(CARD_LOGIN);
//        }
//    }

    public void switchCard(String toCard){
        layout.show(mainPanel, toCard);
        currentCard = toCard;
    }

    @Override
    public boolean loginSuccessful(String login, String pass) {
        Long uId = userController.logUser(login, pass);
        cookies.setLoggedUserId(uId);
        return uId!=null;
    }

    @Override
    public void loginUnsuccessful() {
        switchCard(CARD_LOGIN);
    }

    @Override
    public boolean tryRegister(String login, String pass) {
        return userController.registerUser(login, pass);
//        switchCard(CARD_LOGIN);
    }

    @Override
    public boolean addTask(String category, String task) {

        return taskController.addTask(category,task, cookies.getLoggedUserId());
    }

    @Override
    public List<Task> refreshList() {
        List<Task> tasks = taskController.printTask(cookies.getLoggedUserId());
        return tasks;
    }

    @Override
    public void removeTask(long id) {
        taskController.removeTask(id);
    }
}
