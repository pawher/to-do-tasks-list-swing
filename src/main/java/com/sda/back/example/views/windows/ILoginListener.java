package com.sda.back.example.views.windows;

public interface ILoginListener {
    boolean loginSuccessful(String login, String password);

    void loginUnsuccessful();
}
