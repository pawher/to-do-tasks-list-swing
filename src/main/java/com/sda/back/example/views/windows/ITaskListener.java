package com.sda.back.example.views.windows;

import com.sda.back.example.model.Task;
import com.sda.back.example.model.User;

import java.util.List;

public interface ITaskListener {
    boolean addTask(String category, String task);
    List<Task> refreshList();
    void removeTask(long id);
}
