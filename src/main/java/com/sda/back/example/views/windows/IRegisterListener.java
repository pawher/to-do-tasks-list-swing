package com.sda.back.example.views.windows;

public interface IRegisterListener {
    boolean tryRegister(String login, String password);
}
