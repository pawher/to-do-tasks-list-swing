package com.sda.back.example.views.windows;

import sun.nio.cs.ext.ISCII91;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import static com.sda.back.example.views.windows.WindowFrame.CARD_APP;
import static com.sda.back.example.views.windows.WindowFrame.CARD_REGISTER;

public class LoginForm {
    private JPanel mainPanel;
    private JTextField loginField;
    private JPasswordField passwordField;
    private JButton Loginbutton;
    private JLabel notificationLabel;
    private JButton registerButton;

    private ILoginListener window;
    private ICardSwitchingListener cardSwitchingListener;

    public LoginForm(ILoginListener window, ICardSwitchingListener lstner) {
        this.window = window;
        this.cardSwitchingListener = lstner;



        Loginbutton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String password = new String(passwordField.getPassword());
                System.out.println("Pass is: " + password);
                boolean wynik = window.loginSuccessful(loginField.getText().toLowerCase(), password);

                if (wynik) {
//                    notificationLabel.setText("Udało sie zalogowac");
                    cardSwitchingListener.switchCard(CARD_APP);
                    loginField.setText("");
                    passwordField.setText("");
                } else {
                    notificationLabel.setText("wrong password or login");

                }
            }
        });
        registerButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                cardSwitchingListener.switchCard(CARD_REGISTER);
            }
        });
    }

    public JPanel getMainPanel() {
        return mainPanel;
    }
}
