package com.sda.back.example.views.windows;

import java.awt.*;

public class MainWindow {


    public static void main(String[] args) {


        EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                WindowFrame frame = new WindowFrame();
                frame.setVisible(true);
            }
        });
    }
}
