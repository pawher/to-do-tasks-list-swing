package com.sda.back.example.views.windows;

import com.sda.back.example.model.Task;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import static com.sda.back.example.views.windows.WindowFrame.CARD_LOGIN;

public class ApplicationView {
    private JPanel mainPanel;
    private JButton wylogujButton;
    private JTextField categoryField;
    private JTextField taskField;
    private JList list1;
    private JButton addTaskButton;
    private JButton removeButton;
    private JButton refreshButton;
    private JComboBox categoryBox1;


    DefaultListModel listModel;

    private ICardSwitchingListener cardSwitchingListener;


    public ApplicationView(ICardSwitchingListener cardSwitchingListener, ITaskListener taskListener) {
        listModel = new DefaultListModel();
        list1.setModel(listModel);
        categoryBox1.addItem("zakupy   ");
        categoryBox1.addItem("dom      ");
        categoryBox1.addItem("rozrywka ");
        categoryBox1.addItem("inne     ");

        wylogujButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                cardSwitchingListener.switchCard(CARD_LOGIN);
//                categoryField.setText("");
                taskField.setText("");


            }
        });
        addTaskButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                taskListener.addTask(categoryBox1.getSelectedItem().toString(), taskField.getText());
            }
        });
        refreshButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                List<Task> taskList = taskListener.refreshList();

                listModel.removeAllElements();
                for (Task t : taskList) {
                    listModel.addElement(t);
                }
            }
        });
        removeButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Task selectedTask = (Task) list1.getSelectedValue();
                int selectedID = list1.getSelectedIndex();
                // jest zaznaczone
                if (selectedID != -1) {
                    listModel.remove(selectedID);
                    System.out.println(selectedTask.getId());
                    taskListener.removeTask(selectedTask.getId());
                }
            }


        });


    }


    public JPanel getMainPanel() {
        return mainPanel;
    }

}
