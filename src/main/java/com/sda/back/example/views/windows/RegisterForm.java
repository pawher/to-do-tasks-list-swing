package com.sda.back.example.views.windows;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import static com.sda.back.example.views.windows.WindowFrame.CARD_LOGIN;
import static java.awt.image.ImageObserver.PROPERTIES;

public class RegisterForm {
    private JPanel mainPanel;
    private JTextField RejField;
    private JPasswordField passwordFieldRej;
    private JButton registerButton;
    private JLabel notificationLabel;
    private JButton backButton;

    private IRegisterListener window;
    private ICardSwitchingListener cardSwitchingListener;



    public RegisterForm(IRegisterListener window, ICardSwitchingListener listener) {
        this.window = window;
        this.cardSwitchingListener = listener;

        registerButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {


                boolean wynik = window.tryRegister(RejField.getText().toLowerCase(), new String(passwordFieldRej.getPassword()));
                if (wynik) {
                    notificationLabel.setText("Udalo sie.");
                    cardSwitchingListener.switchCard(CARD_LOGIN);
                    RejField.setText("");
                    passwordFieldRej.setText("");

                } else {
                    notificationLabel.setText("Blad, nie udalo sie zarejestrowac.");
                }
            }
        });
        backButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                cardSwitchingListener.switchCard(CARD_LOGIN);

            }
        });
    }

    public JPanel getMainPanel() {
        return mainPanel;
    }
}
