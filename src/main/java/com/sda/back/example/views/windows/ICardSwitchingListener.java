package com.sda.back.example.views.windows;

public interface ICardSwitchingListener {
    void switchCard(String toCard);
}
