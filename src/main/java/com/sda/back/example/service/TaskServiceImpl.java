package com.sda.back.example.service;

import com.sda.back.example.model.Task;
import com.sda.back.example.model.User;

import java.util.*;

public class TaskServiceImpl implements TaskService {


    private Map<Long, Task> tasks = new HashMap<>();
    private static long taskIDCounter = 1;


    @Override
    public boolean addTask(String name, String text, long userId) {
        Task newTask = new Task(name, text, userId, taskIDCounter++);
        tasks.put(newTask.getId(), newTask);

        return true;
    }

    @Override
    public List<Task> printList(long userID) {
        Collection<Task> tasksSet = tasks.values();
        List<Task> tasksList = new ArrayList<>();
        for (Task task : tasksSet) {
            if (userID == task.getOwnerId()) {
                tasksList.add(task);
            }
        }
        return tasksList;
    }


    @Override
    public void removeTask(long id) {
        tasks.remove(id);

    }


}
