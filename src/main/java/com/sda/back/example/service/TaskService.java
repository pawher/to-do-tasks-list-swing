package com.sda.back.example.service;

import com.sda.back.example.model.Task;
import com.sda.back.example.model.User;

import java.util.List;

public interface TaskService {
    boolean addTask(String name, String text, long userId);
    List<Task> printList(long userID);
    void removeTask (long id);

}
