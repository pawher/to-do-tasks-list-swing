package com.sda.back.example.service;

import com.sda.back.example.model.User;
import org.apache.commons.codec.digest.DigestUtils;

import javax.xml.bind.DatatypeConverter;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.*;

public class UserServiceImpl implements UserService {

    private static long USER_ID_COUNTER = 1;

    private Map<Long, User> users = new HashMap<>();


    public User getUserWithId(long id) {
        return users.get(id);
    }

    public boolean registerUser(String login, String password) {

        if (userExists(login)) {
            return false;
        }
        try {
            MessageDigest digest = MessageDigest.getInstance("MD5");

            String hashed = DigestUtils.md5Hex(password).toLowerCase();

            System.out.println("Encrypted password is: " + hashed);

            User newUser = new User(USER_ID_COUNTER++, login, hashed);

            users.put(newUser.getId(), newUser);
            return true;
        } catch (NoSuchAlgorithmException e) {
            System.err.println("Unable to encrypt password - no such algorithm.");
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public Long logUser(String login, String password) {
        try {
            MessageDigest digest = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        String hashed = DigestUtils.md5Hex(password).toLowerCase();
        Collection<User> values = users.values();
        for (User u: values){
            String login1 = u.getLogin();
            if (login.equals(login1)){
               if(u.getPasswordHash().equals(hashed)){
                   u.setLog(true);
                   return u.getId();
               }
            }
        }
        System.out.println("nie ma takiego uzytkownika lub bledne haslo");

        return null;
    }



    @Override
    public boolean userExists(String login) {
        return false;
    }
}
