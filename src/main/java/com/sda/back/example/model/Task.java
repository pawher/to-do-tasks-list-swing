package com.sda.back.example.model;

import java.util.List;

public class Task {
    private String taskName;
    private String text;
    private long ownerId;
    private long id;

    public Task(String taskName, String text, long ownerId, long id) {
        this.taskName = taskName;
        this.text = text;
        this.ownerId = ownerId;
        this.id = id;
    }

    public Task() {
    }

    public long getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(long ownerId) {
        this.ownerId = ownerId;
    }

    public String getTaskName() {
        return taskName;
    }

    public void setTaskName(String taskName) {
        this.taskName = taskName;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }


    @Override
    public String toString() {

        return taskName + " " + text;
    }
}
