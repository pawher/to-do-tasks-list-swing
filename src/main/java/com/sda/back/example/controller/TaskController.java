package com.sda.back.example.controller;

import com.sda.back.example.model.Task;
import com.sda.back.example.model.User;
import com.sda.back.example.service.TaskService;
import com.sda.back.example.service.TaskServiceImpl;

import java.util.List;

public class TaskController {
    private TaskService taskService = new TaskServiceImpl();

    public boolean addTask(String name, String text, long userId){
        if(taskService.addTask(name, text, userId)){
            System.out.println("Udało Ci się dodać taska");
            return true;
        }else{
            System.err.println("Nie udało sie :(");
        }
        return false;
    }

    public List<Task> printTask(long userId) {
       return taskService.printList(userId);

    }

    public void removeTask (long id) {
        taskService.removeTask(id);
        System.out.println("Udalo sie usunac taska");
    }



}
