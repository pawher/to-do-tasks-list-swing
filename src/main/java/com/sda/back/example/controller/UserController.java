package com.sda.back.example.controller;

import com.sda.back.example.service.UserService;
import com.sda.back.example.service.UserServiceImpl;

import java.util.Optional;

public class UserController {

    private UserService userService = new UserServiceImpl();

    public boolean registerUser(String login, String password){
        return userService.registerUser(login, password);
    }

    public Long logUser(String login, String password){
        Long uId = userService.logUser(login, password);
        if(uId != null){
            System.out.println("Udało Ci się zalogowac");
            return uId;
        }else{
            System.err.println("Nie udało się zalogowac :(");
            return null;
        }

    }

    public UserService getUserService() {
        return userService;
    }
}
